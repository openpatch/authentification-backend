# Authentification Service

API Documentation: https://openpatch.gitlab.io/authentification-backend

## Environment Variables

| Environment Variable | Description | Default | Values |
| --- | ----------- | ------- | -------- |
| OPENPATCH_MOCK | Fill the database with mock data | not set | "true"  |
| OPENPATCH_MODE | Configures the configuration | "production" | "development", "production", "testing" |
| OPENPATCH_DB | URI for connecting to a database | sqlite+pysqlite:///database.db | <protocol\>://\<path\> |
| OPENPATCH_ADMIN_PASSWORD | Admin password used for initial migration | random - will be printed to stdout | anything |
| OPENPATCH_SECRET | Phrase used for hashing | "not-save" | anything |
| OPENPATCH_JWT Default | Phrase used for signing jwt tokens | "not-save" | anything |
| OPENPATCH_ORIGINS | Allowed CORS origins | "*" | valid cors origin |
| SENTRY_DSN | Error tracking with Sentry | not set | a valid dsn |
| OPENPATCH_DISABLE_REGISTRATION | Limits registration to invite only | | "true" |
| RABBITMQ_HOST | URI for connecting to rabbitmq | not set (required) | amqp://\<path\> |

## Start

```
docker-compose up
```

## Testing

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend flask test
```

## Coverage

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend python3 coverage_report.py
```

## ER-Diagram

![ER-Diagram](.gitlab/er_diagram.png)
