from flask_testing import TestCase
from openpatch_authentification import create_app
from openpatch_core.database import db
from tests.mock_db import mock, members
from unittest.mock import MagicMock
from openpatch_core.rabbitmq import RabbitMQ

RabbitMQ.publish = MagicMock()
RabbitMQ.close = MagicMock()
RabbitMQ.connect = MagicMock()


class BaseTest(TestCase):
    def create_app(self):
        return create_app("testing")

    def setUp(self, login_path):
        db.drop_all()
        db.create_all()
        mock()

        self.members = {}

        def add_token_for_member(username, password):
            json = {"username": username, "password": password}
            response = self.client.post(login_path, json=json)

            access_token = response.json.get("access_token")
            refresh_token = response.json.get("refresh_token")

            self.members[username] = {
                "access_token": access_token,
                "refresh_token": refresh_token,
                "login_credentials": json,
                "access_header": {"Authorization": "Bearer {}".format(access_token)},
                "refresh_header": {"Authorization": "Bearer {}".format(refresh_token)},
            }

        for member in members:
            username = member["username"]
            add_token_for_member(username, username)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
