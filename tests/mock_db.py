import uuid
import datetime
from openpatch_authentification.models.member import Member
from openpatch_authentification.models.role import Role
from openpatch_authentification.models.invitation_code import InvitationCode
from openpatch_authentification.models.policy import Policy
from openpatch_authentification.models.member_policy import MemberPolicy

from openpatch_core.database import db


def to_datetime(timestamp):
    return datetime.datetime.fromisoformat(timestamp)


roles = [
    {"id": "c093b3ee-316b-4374-bc8d-ab65384d47d3", "type": "user"},
    {"id": "7c76dbb7-b6d2-46f5-90c9-8067fb249bd4", "type": "editor"},
    {"id": "91d8d3c7-a2d9-43c8-bfd8-98b6a277c909", "type": "analyst"},
    {"id": "81dec5f2-b740-4fb0-b379-3766818a4c1e", "type": "admin"},
]

invitation_codes = [uuid.uuid4()]


members = [
    {
        "id": "250512d6-16e8-4161-8ac2-43501a7efe28",
        "username": "admin",
        "email": "admin@openpatch.org",
        "full_name": "Admin Admin",
        "confirmed": True,
        "role_id": roles[3]["id"],
    },
    {
        "id": "d0c54373-e1d4-4ad3-8043-66d1b27752ff",
        "username": "analyst",
        "email": "analyst@openpatch.org",
        "full_name": "Analyst Analyst",
        "confirmed": True,
        "role_id": roles[2]["id"],
    },
    {
        "id": "80bc9049-6ad5-40c9-9568-67db86e24029",
        "username": "editor",
        "email": "editor@editor.de",
        "full_name": "Editor Editor",
        "confirmed": True,
        "role_id": roles[1]["id"],
    },
    {
        "id": "8a235886-2ef5-4d02-a117-f113c2023b41",
        "username": "user",
        "email": "user@user.de",
        "full_name": "User User",
        "confirmed": True,
        "role_id": roles[0]["id"],
    },
    {
        "id": "eba74813-98cf-401d-9963-7344342069ca",
        "username": "unconfirmed_user",
        "email": "user_unconfirmed@user.de",
        "full_name": "No User",
        "confirmed": False,
        "role_id": roles[0]["id"],
    },
]

policies = [
    {
        "id": "99708a59-ea81-4d2f-83e0-80337826633a",
        "content": {"text": "GDPR 0"},
        "type": "gdpr",
        "created_on": to_datetime("2020-02-17T09:38:33.631"),
    },
    {
        "id": "f1273baa-d6b5-48b1-9332-5054bec7cbf7",
        "content": {"text": "GDPR 1"},
        "type": "gdpr",
        "created_on": to_datetime("2020-02-18T09:38:33.631"),
    },
    {
        "id": "e3dd20f2-60f9-441e-8854-ac6568faf67e",
        "content": {"text": "GDPR 2"},
        "type": "gdpr",
        "created_on": to_datetime("2020-02-19T09:38:33.631"),
    },
    {
        "id": "79a472e9-38ba-4c1c-a618-cf7795e20463",
        "draft": False,
        "content": {"text": "GDPR 3"},
        "type": "gdpr",
        "created_on": to_datetime("2020-02-20T09:38:33.631"),
    },
    {
        "id": "f4e4b95f-58d8-48c5-9ae2-a99b7f946c9c",
        "draft": True,
        "content": {"text": "GDPR 4"},
        "type": "gdpr",
        "created_on": to_datetime("2020-02-21T09:38:33.631"),
    },
    {
        "id": "fde39649-971b-4c48-8d50-9f19c7578b92",
        "content": {"text": "Cookie 0"},
        "type": "cookie",
        "created_on": to_datetime("2020-02-21T09:38:33.631"),
    },
    {
        "id": "01580d52-7bf6-4448-93e7-cb79ea2250a9",
        "content": {"text": "Cookie 1"},
        "type": "cookie",
        "draft": True,
        "created_on": to_datetime("2020-02-25T09:38:33.631"),
    },
]

members_policies = [
    {
        "id": "c5152781-d6cf-4de1-a798-571e25f25909",
        "member_id": members[2]["id"],
        "policy_id": policies[2]["id"],
        "accepted": False,
    },
    {
        "id": "1e7fe3aa-a044-4e71-bd55-1a343ee01148",
        "member_id": members[3]["id"],
        "policy_id": policies[3]["id"],
        "accepted": True,
    },
    {
        "id": "38c065b0-8b96-47df-9be4-58cc048ab49a",
        "member_id": members[3]["id"],
        "policy_id": policies[5]["id"],
        "accepted": True,
    },
]


def mock():
    for role in roles:
        db.session.add(Role(**role))

    for member in members:
        m = Member(**member)
        m.hash_password(member["username"])
        db.session.add(m)

    for policy in policies:
        g = Policy(**policy)
        db.session.add(g)

    for member_policy in members_policies:
        g = MemberPolicy(**member_policy)
        db.session.add(g)

    for invitation_code in invitation_codes:
        ic = InvitationCode(id=invitation_code, member_id=members[0]["id"])
        db.session.add(ic)

    db.session.commit()
