import os
from tests.base_test import BaseTest


class TestMembers(BaseTest):
    def setUp(self):
        super(TestMembers, self).setUp("/v1/login")

    def test_get_members(self):
        def test_not_admin(username):
            response = self.client.get(
                "/v1/members", headers=self.members[username]["access_header"]
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn("members", response.json)
            self.assertIsInstance(response.json["members"], list)
            self.assertTrue(len(response.json["members"]) == 1)
            self.assertTrue(response.json["members"][0]["username"] == username)

        test_not_admin("user")
        test_not_admin("editor")
        test_not_admin("analyst")

        response = self.client.get(
            "/v1/members", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)

        self.assertIn("members", response.json)
        self.assertIsInstance(response.json["members"], list)
        self.assertTrue(len(response.json["members"]) > 1)

    def test_post_member(self):
        def test_not_admin(username):
            response = self.client.post(
                "/v1/members", headers=self.members[username]["access_header"]
            )
            self.assertEqual(response.status_code, 403)

        test_not_admin("user")
        test_not_admin("editor")
        test_not_admin("analyst")

        member_data = {
            "username": "test",
            "full_name": "test test",
            "password": "test_test",
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 200)
        self.assertIn("member_id", response.json)

        member_data = {
            "username": "test",
            "full_name": "test test",
            "password": "test",
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": "test_8",
            "full_name": "test test",
            "password": "test",
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": None,
            "full_name": "test test",
            "password": "test",
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": "test_2",
            "full_name": None,
            "password": "test",
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": "test_3",
            "full_name": "test test",
            "password": None,
            "email": "success@simulator.amazonses.com",
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": "test_4",
            "full_name": "test test",
            "password": "test",
            "email": None,
            "url": "http://openpatch.app/confirm-email/{}",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

        member_data = {
            "username": "test_4",
            "full_name": "test test",
            "password": "test",
            "email": "test",
        }

        response = self.client.post(
            "v1/members",
            json=member_data,
            headers=self.members["admin"]["access_header"],
        )

        self.assertEqual(response.status_code, 400)

    def test_put_member_by_id(self):
        def test_not_admin(username):
            response = self.client.put(
                "/v1/members/1", headers=self.members[username]["access_header"]
            )
            self.assertEqual(response.status_code, 403)

        test_not_admin("user")
        test_not_admin("editor")
        test_not_admin("analyst")

    def test_get_member_by_id(self):
        def test_not_admin(username):
            response = self.client.get(
                "/v1/members/250512d6-16e8-4161-8ac2-43501a7efe28",
                headers=self.members[username]["access_header"],
            )
            self.assertEqual(response.status_code, 200)

        test_not_admin("user")
        test_not_admin("editor")
        test_not_admin("analyst")

    def test_delete_member_by_id(self):
        def test_not_admin(username):
            response = self.client.delete(
                "/v1/members/250512d6-16e8-4161-8ac2-43501a7efe28",
                headers=self.members[username]["access_header"],
            )
            self.assertEqual(response.status_code, 403)

        test_not_admin("user")
        test_not_admin("editor")
        test_not_admin("analyst")

    def test_post_inivation_code(self):
        response = self.client.post(
            "/v1/members/250512d6-16e8-4161-8ac2-43501a7efe28/invitation-codes",
            headers=self.members["admin"]["access_header"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.get_json().get("invitation_code"))

