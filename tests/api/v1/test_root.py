import os
from openpatch_authentification.models.member_policy import MemberPolicy
from openpatch_authentification.models.member import Member
from tests.base_test import BaseTest
from tests import mock_db


class TestRoot(BaseTest):
    def setUp(self):
        super(TestRoot, self).setUp("/v1/login")

    def test_login(self):
        response = self.client.get("/v1/login")
        self.assertEqual(response.status_code, 405)

        response = self.client.post("/v1/login", json={"username": "test"})
        self.assertEqual(response.status_code, 401)

        response = self.client.post(
            "/v1/login", json=self.members["unconfirmed_user"]["login_credentials"]
        )
        self.assertEqual(response.status_code, 422)

        response = self.client.post(
            "/v1/login", json=self.members["admin"]["login_credentials"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.json.get("access_token"))
        self.assertIsNotNone(response.json.get("refresh_token"))

    def test_register(self):
        if os.environ["OPENPATCH_DISABLE_REGISTRATION"]:
            del os.environ["OPENPATCH_DISABLE_REGISTRATION"]
        member_json = {
            "username": "new_member",
            "email": "success@simulator.amazonses.com",
            "password": "new_member",
            "full_name": "New Member",
            "url": "http://openpatch.app/confirm-email/{}",
            "policy": mock_db.policies[3]["id"],
        }

        incorrect_member_json = {"username": "new_member"}

        incorrect_password_json = {
            "username": "new_member1",
            "password": "test",
            "email": "new@new.de",
        }

        duplicate_member_json = {
            "username": "admin",
            "password": "test_test",
            "email": "admin@admin.de",
        }

        response = self.client.post("/v1/register", json=incorrect_password_json)
        self.assertEqual(response.status_code, 400)

        response = self.client.post("/v1/register", json=incorrect_member_json)
        self.assertEqual(response.status_code, 400)

        response = self.client.post("/v1/register", json=duplicate_member_json)
        self.assertEqual(response.status_code, 400)

        response = self.client.post("/v1/register", json=member_json)
        self.assertEqual(response.status_code, 200)

        member = Member.query.filter_by(username="new_member").first()
        member_policy = MemberPolicy.query.filter_by(member=member).first()
        self.assertIsNotNone(member_policy)
        self.assertTrue(member_policy.accepted)

    def test_disabled_register(self):
        os.environ["OPENPATCH_DISABLE_REGISTRATION"] = "true"
        member_json = {
            "username": "new_member",
            "email": "success@simulator.amazonses.com",
            "password": "new_member",
            "full_name": "New Member",
            "url": "http://openpatch.app/confirm-email/{}",
        }
        response = self.client.post("/v1/register", json=member_json)
        self.assertEqual(response.status_code, 400)

        member_json["invitation_code"] = mock_db.invitation_codes[0]
        response = self.client.post("/v1/register", json=member_json)
        self.assertEqual(response.status_code, 200)

    def test_refresh(self):
        response = self.client.get("/v1/refresh")
        self.assertEqual(response.status_code, 401)

        response = self.client.get("/v1/refresh", json={"username": "test"})
        self.assertEqual(response.status_code, 401)

        response = self.client.get(
            "/v1/refresh", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 422)

        response = self.client.get(
            "/v1/refresh", headers=self.members["admin"]["refresh_header"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.json.get("access_token"))

    def test_logout(self):
        response = self.client.get("/v1/logout")
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            "/v1/logout", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            "/v1/logout", headers=self.members["admin"]["refresh_header"]
        )
        self.assertEqual(response.status_code, 200)

        # it should be allowed to successfully logout again
        response = self.client.get(
            "/v1/logout", headers=self.members["admin"]["refresh_header"]
        )
        self.assertEqual(response.status_code, 200)

        # after logout the member should not have access
        response = self.client.get(
            "/v1/members", headers=self.members["admin"]["refresh_header"]
        )
        self.assertEqual(response.status_code, 422)

    def test_verify(self):
        response = self.client.get("/v1/verify")
        self.assertEqual(response.status_code, 401)

        response = self.client.get(
            "/v1/verify", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)

    def test_new_policies(self):
        response = self.client.get(
            "/v1/new-policies", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("policies", response.json)
        self.assertEqual(len(response.json.get("policies")), 2)

        response = self.client.get(
            "/v1/new-policies", headers=self.members["user"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("policies", response.json)
        self.assertEqual(len(response.json.get("policies")), 0)

