import os
from tests.base_test import BaseTest
from openpatch_authentification.models.policy import Policy
from openpatch_authentification.models.member_policy import MemberPolicy
from tests import mock_db


class TestPolicies(BaseTest):
    def setUp(self):
        super(TestPolicies, self).setUp("/v1/login")

    def test_post_policies(self):
        policy_json = {
            "content": {"text": "New Policy"},
            "language": "en",
            "type": "terms-of-serivce",
        }
        response = self.client.post(
            "v1/policies",
            headers=self.members["user"]["access_header"],
            json=policy_json,
        )
        self.assertEqual(response.status_code, 403)

        response = self.client.post(
            "v1/policies",
            headers=self.members["admin"]["access_header"],
            json=policy_json,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("policy_id", response.json)
        policy = Policy.query.get(response.json["policy_id"])
        self.assertIsNotNone(policy)

    def test_get_policies(self):
        response = self.client.get(
            "v1/policies", headers=self.members["user"]["access_header"]
        )
        self.assertEqual(response.status_code, 403)

        response = self.client.get(
            "v1/policies", headers=self.members["admin"]["access_header"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("policies", response.json)
        self.assertEqual(len(response.json["policies"]), len(mock_db.policies))

    def test_get_latest(self):
        response = self.client.get("v1/policies/latest")
        self.assertEqual(response.status_code, 404)

        response = self.client.get("v1/policies/latest?type=gdpr")
        self.assertEqual(response.status_code, 200)
        self.assertIn("policy", response.json)
        self.assertIsNotNone(response.json["policy"])

    def test_get_policy_by_id(self):
        response = self.client.get(f"v1/policies/{mock_db.policies[0]['id']}")
        self.assertEqual(response.status_code, 200)
        self.assertIn("policy", response.json)
        self.assertIsNotNone(response.json["policy"])

    def test_put_policy_by_id(self):
        policy_json = {
            "content": {"text": "New Policy"},
            "language": "en",
            "type": "terms-of-serivce",
        }
        response = self.client.put(
            f"v1/policies/{mock_db.policies[0]['id']}",
            headers=self.members["user"]["access_header"],
            json=policy_json,
        )
        self.assertEqual(response.status_code, 403)

        response = self.client.put(
            f"v1/policies/{mock_db.policies[0]['id']}",
            headers=self.members["admin"]["access_header"],
            json=policy_json,
        )
        self.assertEqual(response.status_code, 200)
        policy = Policy.query.get(mock_db.policies[0]["id"])
        self.assertEqual(policy.type, policy_json["type"])

    def test_post_policy_accept(self):
        response = self.client.post(
            f"v1/policies/{mock_db.policies[0]['id']}/accept",
            headers=self.members["admin"]["access_header"],
        )
        self.assertEqual(response.status_code, 200)
        member_policy = MemberPolicy.query.filter_by(
            member_id=mock_db.members[0]["id"], policy_id=mock_db.policies[0]["id"]
        ).first()
        self.assertIsNotNone(member_policy)
        self.assertTrue(member_policy.accepted)

    def test_delete_policy_accept(self):
        response = self.client.delete(
            f"v1/policies/{mock_db.policies[3]['id']}/accept",
            headers=self.members["user"]["access_header"],
        )
        self.assertEqual(response.status_code, 200)
        member_policy = MemberPolicy.query.filter_by(
            member_id=mock_db.members[3]["id"], policy_id=mock_db.policies[3]["id"]
        ).first()
        self.assertIsNotNone(member_policy)
        self.assertFalse(member_policy.accepted)

