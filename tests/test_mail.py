import unittest
from unittest.mock import patch

from openpatch_authentification.mail import send_confirm_email


class TestMail(unittest.TestCase):
    @patch("openpatch_authentification.mail.send_email")
    def test_send_confirm_mail(self, mock_send_email):
        send_confirm_email("test@test.de", "hallo.com", "testuser")

        self.assertTrue(mock_send_email.called)
        args, kwargs = mock_send_email.call_args
        self.assertEqual(args[0], "test@test.de")
        self.assertEqual(args[1], "Confirm your email")
        self.assertIn("testuser", args[2])
