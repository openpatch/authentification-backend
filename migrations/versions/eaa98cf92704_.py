"""init

Revision ID: eaa98cf92704
Revises: 
Create Date: 2020-01-03 20:18:17.602205

"""
import os
import uuid
from alembic import op
import sqlalchemy as sa
import openpatch_core
from passlib.apps import custom_app_context as pwd_context


# revision identifiers, used by Alembic.
revision = 'eaa98cf92704'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('authentification_avatar',
    sa.Column('created_on', sa.DateTime(), nullable=True),
    sa.Column('updated_on', sa.DateTime(), nullable=True),
    sa.Column('id', sa.String(length=254), nullable=False),
    sa.Column('name', sa.Text(), nullable=True),
    sa.Column('content_length', sa.Integer(), nullable=True),
    sa.Column('content_type', sa.String(length=128), nullable=True),
    sa.Column('url', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    role_table = op.create_table('authentification_role',
    sa.Column('created_on', sa.DateTime(), nullable=True),
    sa.Column('updated_on', sa.DateTime(), nullable=True),
    sa.Column('id', openpatch_core.database.types.GUID(), nullable=False),
    sa.Column('type', sa.String(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('type')
    )

    admin_uuid = str(uuid.uuid4())
    op.bulk_insert(
        role_table,
        [
            {"id": admin_uuid, "type": "admin"},
            {"id": str(uuid.uuid4()), "type": "analyst"},
            {"id": str(uuid.uuid4()), "type": "author"},
            {"id": str(uuid.uuid4()), "type": "user"},
            {"id": str(uuid.uuid4()), "type": "editor"},
        ],
    )

    op.create_table('authentification_token_blacklist',
    sa.Column('created_on', sa.DateTime(), nullable=True),
    sa.Column('updated_on', sa.DateTime(), nullable=True),
    sa.Column('token', sa.String(length=1024), nullable=False),
    sa.PrimaryKeyConstraint('token')
    )
    member_table = op.create_table('authentification_member',
    sa.Column('created_on', sa.DateTime(), nullable=True),
    sa.Column('updated_on', sa.DateTime(), nullable=True),
    sa.Column('id', openpatch_core.database.types.GUID(), nullable=False),
    sa.Column('avatar_id', sa.String(length=254), nullable=True),
    sa.Column('confirmed', sa.Boolean(), nullable=True),
    sa.Column('email', sa.String(length=128), nullable=True),
    sa.Column('full_name', sa.String(length=128), nullable=True),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('role_id', openpatch_core.database.types.GUID(), nullable=True),
    sa.Column('password_hash', sa.String(length=512), nullable=True),
    sa.ForeignKeyConstraint(['avatar_id'], ['authentification_avatar.id'], ),
    sa.ForeignKeyConstraint(['role_id'], ['authentification_role.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('username')
    )

    admin_password = os.getenv("OPENPATCH_ADMIN_PASSWORD", None)

    if not admin_password:
        import random
        import string

        admin_password = "".join(
            random.SystemRandom().choice(string.ascii_uppercase + string.digits)
            for _ in range(20)
        )
        print("Admin Password: " + admin_password)

    op.bulk_insert(
        member_table,
        [
            {
                "id": str(uuid.uuid4()),
                "username": "admin",
                "role_id": admin_uuid,
                "confirmed": True,
                "email": "admin@openpatch.org",
                "password_hash": pwd_context.hash(admin_password),
            }
        ],
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('authentification_member')
    op.drop_table('authentification_token_blacklist')
    op.drop_table('authentification_role')
    op.drop_table('authentification_avatar')
    # ### end Alembic commands ###
