"""add invitation code

Revision ID: 6c549be6cceb
Revises: eaa98cf92704
Create Date: 2020-02-27 12:51:44.260229

"""
from alembic import op
import sqlalchemy as sa
import openpatch_core


# revision identifiers, used by Alembic.
revision = '6c549be6cceb'
down_revision = 'eaa98cf92704'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('authentification_invitation_code',
    sa.Column('created_on', sa.DateTime(), nullable=True),
    sa.Column('updated_on', sa.DateTime(), nullable=True),
    sa.Column('id', openpatch_core.database.types.GUID(), nullable=False),
    sa.Column('member_id', openpatch_core.database.types.GUID(), nullable=True),
    sa.Column('used', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['member_id'], ['authentification_member.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('authentification_invitation_code')
    # ### end Alembic commands ###
