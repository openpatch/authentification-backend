## [1.6.5](https://gitlab.com/openpatch/authentification-backend/compare/v1.6.4...v1.6.5) (2021-01-24)


### Bug Fixes

* upgrade flask-microservice-base for solving mysql disconnect ([4dbbc0e](https://gitlab.com/openpatch/authentification-backend/commit/4dbbc0e2d0bcf951a0ca851c1810a185778e988f))

## [1.6.4](https://gitlab.com/openpatch/authentification-backend/compare/v1.6.3...v1.6.4) (2020-06-07)


### Bug Fixes

* use rabbitmq from core ([30febe7](https://gitlab.com/openpatch/authentification-backend/commit/30febe7c8d4edc01876a1ed6a20c11dfdc34cd8c))

## [1.6.3](https://gitlab.com/openpatch/authentification-backend/compare/v1.6.2...v1.6.3) (2020-06-06)


### Bug Fixes

* mocking of data ([4ac06da](https://gitlab.com/openpatch/authentification-backend/commit/4ac06dad8b50807db5e255e2c413add7036098d1))

## [1.6.2](https://gitlab.com/openpatch/authentification-backend/compare/v1.6.1...v1.6.2) (2020-06-05)


### Bug Fixes

* mock before run ([1a2c018](https://gitlab.com/openpatch/authentification-backend/commit/1a2c0183b65e0406688a8eba6799cc40e63a352d))
