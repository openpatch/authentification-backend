FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

COPY "." "/var/www/app"

ENV OPENPATCH_SERVICE_NAME=authentification
ENV OPENPATCH_JWT=not-save
ENV OPENPATCH_SECRET=not-save
ENV OPENPATCH_DB=sqlite+pysqlite:///database.db
ENV OPENPATCH_ORIGINS=*

RUN pip3 install -r /var/www/app/requirements.txt
