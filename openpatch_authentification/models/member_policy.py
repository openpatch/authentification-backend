from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class MemberPolicy(Base):
    __tablename__ = gt("member_policy")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    member_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("member")))
    policy_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("policy")))

    accepted = db.Column(db.Boolean)

    member = db.relationship("Member", back_populates="member_policy")
    policy = db.relationship("Policy", back_populates="member_policy")
