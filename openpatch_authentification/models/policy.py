from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class Policy(Base):
    __tablename__ = gt("policy")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    content = db.Column(db.JSON)
    draft = db.Column(db.Boolean, default=False)
    language = db.Column(db.String(32), default="en")
    type = db.Column(db.String(256))
    member_policy = db.relationship("MemberPolicy", back_populates="policy")
