from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class Role(Base):
    __tablename__ = gt("role")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    type = db.Column(db.String(64), unique=True)
    members = db.relationship("Member", back_populates="role")
