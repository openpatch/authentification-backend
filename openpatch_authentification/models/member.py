from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from passlib.apps import custom_app_context as pwd_context
import uuid


class Member(Base):
    __tablename__ = gt("member")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    avatar_id = db.Column(GUID())
    confirmed = db.Column(db.Boolean)
    email = db.Column(db.String(128), unique=True)
    full_name = db.Column(db.String(128))
    username = db.Column(db.String(64), unique=True)
    role_id = db.Column(GUID(), db.ForeignKey("%s.id" % gt("role")))
    role = db.relationship("Role", back_populates="members")
    password_hash = db.Column(db.String(512))
    invitation_codes = db.relationship("InvitationCode", back_populates="member")
    member_policy = db.relationship("MemberPolicy", back_populates="member")

    def hash_password(self, password):
        self.password_hash = pwd_context.hash(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def get_claims_for_access_token(self):
        return {
            "id": self.id,
            "avatar_id": self.avatar_id,
            "username": self.username,
            "role": self.role.type,
            "full_name": self.full_name,
            "email": self.email,
            "confirmed": self.confirmed,
        }

    def as_dict(self):
        return self.get_claims_for_access_token()
