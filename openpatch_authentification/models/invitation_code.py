from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class InvitationCode(Base):
    __tablename__ = gt("invitation_code")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(db.ForeignKey("%s.id" % gt("member")))
    member = db.relationship("Member", back_populates="invitation_codes")
    used = db.Column(db.Boolean)
