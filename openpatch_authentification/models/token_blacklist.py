from openpatch_core.database import db, gt
from openpatch_core.models import Base


class TokenBlacklist(Base):
    __tablename__ = gt("token_blacklist")

    token = db.Column(db.String(1024), primary_key=True)
