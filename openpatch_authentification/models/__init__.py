from openpatch_authentification.models import (
    member,
    role,
    invitation_code,
    token_blacklist,
    policy,
    member_policy,
)
from openpatch_core.database import db
from sqlalchemy import orm

orm.configure_mappers()
