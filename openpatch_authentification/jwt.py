from flask_jwt_extended import JWTManager
from openpatch_authentification.models.token_blacklist import TokenBlacklist
from openpatch_core.database import db

jwt = JWTManager()

"""
@apiDefine jwt
@apiHeader {String} Authorization Bearer <code>token</code>
"""


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token["jti"]
    token = TokenBlacklist.query.get(jti)
    return token is not None


def blacklist_token(jti):
    token = TokenBlacklist(token=jti)
    db.session.add(token)
    db.session.commit()


@jwt.user_claims_loader
def add_claims_to_access_token(member):
    return member.get_claims_for_access_token()


@jwt.user_identity_loader
def member_identity_lookup(member):
    return member.username
