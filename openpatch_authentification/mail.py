import os

from openpatch_core.rabbitmq import RabbitMQ
from itsdangerous import URLSafeTimedSerializer

sEmail = URLSafeTimedSerializer(os.getenv("OPENPATCH_SECRET", "not-safe"), salt="email")
sPwd = URLSafeTimedSerializer(
    os.getenv("OPENPATCH_SECRET", "not-safe"), salt="password"
)

rabbit = RabbitMQ()


def send_email(email, subject, body):
    rabbit.publish_json(
        {"to": email, "subject": subject, "text": body},
        exchange="mail_exchange",
        routing_key="mail_queue",
        durable=True,
    )


def send_reset_password_email(email, url):
    token = sPwd.dumps(email)
    link = url.format(token)

    subject = "Reset password"
    body = (
        "You told us you forgot your password."
        "If you really did, click here to choose a new one:\n\n"
        "{}"
        "\nThis link is valid for 1 hour.\n\nIf you didn't mean to reset your password, "
        "then you can just ignore this email, "
        "your password will not change.".format(link)
    )

    send_email(email, subject, body)


def send_confirm_email(email, url, username):
    token = sEmail.dumps(email)
    link = url.format(token)

    subject = "Confirm your email"
    body = (
        f"Hello {username}! Once you've confirmed your email address, "
        "you'll be the newest member of the OpenPatch community.\n\n"
        "{}"
        "\nThis link is valid for 1 day.".format(link)
    )

    send_email(email, subject, body)
