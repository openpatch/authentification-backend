""" api v1 routes for members """
import os
import re
import uuid

from flask import jsonify, request
from flask_jwt_extended import get_jwt_claims, jwt_required
from openpatch_authentification.api.v1 import api
from openpatch_authentification.api.v1 import errors
from openpatch_authentification.mail import send_confirm_email
from openpatch_authentification.models.member import Member
from openpatch_authentification.models.invitation_code import InvitationCode
from openpatch_authentification.models.role import Role
from openpatch_core.database import db


def is_valid_password(password):
    return len(password) > 8 and len(password) < 64


BASE_URL = "/members"

# following simplified RFC 2822 see https://stackoverflow.com/a/1373724/4205043
EMAIL_REGEX = re.compile(
    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
)


@api.route(BASE_URL, methods=["POST", "GET"])
@jwt_required
def root_members():
    """
    redirects based on http method
    """
    jwt_claims = get_jwt_claims()
    if request.method == "POST":
        return post_member(jwt_claims)
    return get_members(jwt_claims)


def post_member(jwt_claims):
    """
    @api {post} /v1/members Post member
    @apiVersion 1.0.0
    @apiName PostMember
    @apiGroup Members

    @apiUse jwt

    @apiParam {String} username The username.
    @apiParam {String{8..64}} password The password.
    @apiParam {String} full_name The full name.
    @apiParam {String} email The e-mail.
    @apiParam {String="admin","author","analyst","user"} [role="user"] The role. If not set defaults to the user role.

    @apiSuccess {UUID} member_id The member id.

    @apiUse errors_access_not_allowed
    @apiUse errors_information_is_missing
    @apiUse errors_username_is_in_use
    @apiUse errors_email_is_in_use
    @apiUse errors_email_is_invalid

    @apiPermission admin
    """
    jwt_claim_role = jwt_claims.get("role")
    json = request.get_json() or {}

    if jwt_claim_role != "admin":
        return errors.access_not_allowed()

    username = json.get("username")
    if not username:
        return errors.information_is_missing()
    if Member.query.filter_by(username=username).first():
        return errors.username_is_in_use()

    password = json.get("password")
    if not password:
        return errors.information_is_missing()
    if not is_valid_password(password):
        return errors.password_is_invalid()

    email = json.get("email")
    if Member.query.filter_by(email=email).first():
        return errors.email_is_in_use()

    role_type = json.get("role_type")
    role = Role.query.filter_by(type=role_type).first()
    if not role:
        role = Role.query.filter_by(type="user").first()

    full_name = json.get("full_name")

    member = Member(username=username, full_name=full_name, email=email, role=role)
    member.hash_password(password)

    url = json.get("url", None)

    send_confirm_email(email, url, username)

    db.session.add(member)
    db.session.commit()

    return jsonify({"member_id": member.id})


def get_members(jwt_claims):
    """
    @api {get} /v1/members Get members
    @apiVersion 1.0.0
    @apiName GetMembers
    @apiGroup Members

    @apiUse jwt

    @apiSuccess {Object[]} members The list of members.
    @apiSuccess {UUID} members.id The member id.
    @apiSuccess {String} members.username The member username.
    @apiSuccess {String} members.role The member role.
    @apiSuccess {String} members.full_name The member full_name.
    @apiSuccess {String} members.email The member e-mail.
    @apiSuccess {Boolean} members.confirmed True if the e-mail is confirmed.
    """
    jwt_claim_role = jwt_claims.get("role")
    jwt_claim_id = jwt_claims.get("id")

    member_query, count, page = Member.elastic_query(request.args.get("query", "{}"))

    if jwt_claim_role != "admin":
        member_query = member_query.filter(Member.id == jwt_claim_id)

    member_query = page(query=member_query)
    members = member_query.all()

    return (
        jsonify(
            {
                "members_count": count,
                "members": [member.as_dict() for member in members],
            }
        ),
        200,
    )


@api.route(BASE_URL + "/<member_id>", methods=["PUT", "GET", "DELETE"])
def member_by_id(member_id):
    """
    redirects based on http method
    """
    if not member_id:
        errors.resource_not_found()

    jwt_claims = get_jwt_claims()
    if request.method == "PUT":
        return put_member_by_id(member_id, jwt_claims)
    if request.method == "DELETE":
        return delete_member_by_id(member_id, jwt_claims)
    return get_member_by_id(member_id, jwt_claims)


@jwt_required
def put_member_by_id(member_id, jwt_claims):
    """
    @api {put} /v1/members/:id Put member by id
    @apiVersion 1.0.0
    @apiName PutMemberById
    @apiGroup Members

    @apiUse jwt

    @apiParam {UUID} id The member id.
    @apiParam {String} username The username.
    @apiParam {String{8..64}} password The password.
    @apiParam {String} full_name The full name.
    @apiParam {String} email The e-mail.
    @apiParam {String="admin","author","analyst","user"} [role="user"] The role. If not set defaults to the user role.

    @apiUse errors_access_not_allowed
    @apiUse errors_resource_not_found
    @apiUse errors_need_one_admin
    @apiUse errors_password_is_invalid
    @apiUse errors_email_is_in_use
    @apiUse errors_email_is_invalid
    """
    jwt_claim_role = jwt_claims.get("role")
    jwt_claim_id = jwt_claims.get("id")
    if jwt_claim_role != "admin" and member_id != jwt_claim_id:
        return errors.access_not_allowed()

    json = request.get_json() or {}

    member = Member.query.get(member_id)
    if not member:
        return errors.resource_not_found()

    new_full_name = json.get("full_name")
    if new_full_name:
        member.full_name = new_full_name

    new_role_type = json.get("role")
    if new_role_type:
        new_role = Role.query.filter_by(type=new_role_type).first()

        if not new_role:
            return errors.resource_not_found()

        if (
            member.role.type == "admin"
            and new_role.type != "admin"
            and Member.query.join(Role).filter_by(type="admin").count() <= 1
        ):
            return errors.need_one_admin()

        member.role = new_role

    new_password = json.get("password")
    if new_password:
        if not is_valid_password(new_password):
            return errors.password_is_invalid()
        member.hash_password(new_password)

    new_email = json.get("email")
    if new_email:
        old_email = member.email
        member.email = new_email
        if old_email != new_email:
            if Member.query.filter_by(email=new_email).first():
                return errors.email_is_in_use()
            url = json.get("url", None)
            send_confirm_email(new_email, url)
            member.confirmed = False

    db.session.commit()
    return jsonify({"status": "success"}), 200


def get_member_by_id(member_id, jwt_claims):
    """
    @api {get} /v1/members/:id Get member by id
    @apiVersion 1.0.0
    @apiName GetMemberById
    @apiGroup Members

    @apiUse jwt

    @apiParam {UUID} id The member id.

    @apiSuccess {Object} member The member.
    @apiSuccess {UUID} member.id The member id.
    @apiSuccess {String} member.username The member username.
    @apiSuccess {String} member.role The member role.
    @apiSuccess {String} member.full_name The member full_name.
    @apiSuccess {String} member.email The member e-mail.
    @apiSuccess {Boolean} member.confirmed True if the e-mail is confirmed.

    @apiUse errors_access_not_allowed
    """
    jwt_claim_role = jwt_claims.get("role")
    jwt_claim_id = jwt_claims.get("id")

    member = Member.query.get(member_id)
    if not member:
        return errors.resource_not_found()

    if jwt_claim_role != "admin" and member_id != jwt_claim_id:
        return jsonify(
            {
                "member": {
                    "id": member.id,
                    "username": member.username,
                    "full_name": member.full_name,
                    "avatar_id": member.avatar_id,
                }
            }
        )

    return jsonify({"member": member.as_dict()}), 200


@jwt_required
def delete_member_by_id(member_id, jwt_claims):
    """
    @api {delete} /v1/members/:id Delete member by id
    @apiVersion 1.0.0
    @apiName DeleteMemberById
    @apiGroup Members

    @apiUse jwt

    @apiParam {UUID} id The member id.

    @apiUse errors_access_not_allowed
    @apiUse errors_resource_not_found
    @apiUse errors_need_one_admin
    """
    jwt_claim_role = jwt_claims.get("role")
    jwt_claim_id = jwt_claims.get("id")

    if jwt_claim_role != "admin" and member_id != jwt_claim_id:
        return errors.access_not_allowed()

    member = Member.query.get(member_id)

    if not member:
        return errors.resource_not_found()

    if member.role.type == "admin" and Member.query.filter_by(type="admin").count <= 1:
        return errors.need_one_admin()

    db.session.delete(member)

    return jsonify({"message": "success"}), 200


@api.route(BASE_URL + "/<member_id>/invitation-codes", methods=["GET", "POST"])
def invitation_codes(member_id):
    if request.method == "GET":
        return get_invitation_codes(member_id)
    elif request.method == "POST":
        return post_invitation_code(member_id)


@jwt_required
def get_invitation_codes(member_id):
    """
    @api {get} /v1/members/:id Get member by id
    @apiVersion 1.0.0
    @apiName GetMemberById
    @apiGroup Members

    @apiUse jwt

    @apiParam {UUID} id The member id.

    @apiSuccess {Object[]} invitation_codes The invitation codes.
    @apiSuccess {UUID} invitation_code.id The id.
    @apiSuccess {Boolean} invitation_code.used True if is used.

    @apiUse errors_resource_not_found
    """
    jwt_claims = get_jwt_claims()

    member = Member.query.get(member_id)
    if not member:
        return errors.resource_not_found()

    invitation_query, count, page = InvitationCode.elastic_query(
        request.args.get("query", "{}")
    )
    invitation_query = invitation_query.filter_by(member=member)
    invitation_query = page(invitation_query)
    invitation_codes = invitation_query.all()

    return jsonify(
        {
            "invitation_codes": [
                {"id": ic.id, "used": ic.used} for ic in invitation_codes
            ]
        }
    )


@jwt_required
def post_invitation_code(member_id):
    """
    @api {post} /v1/members/:id/invitation-codes Post Invitation Code
    @apiVersion 1.0.0
    @apiName PostInivitationCode
    @apiGroup Members

    @apiUse jwt

    @apiUse errors_access_not_allowed
    @apiUse errors_resource_not_found

    @apiPermission admin
    """
    jwt_claims = get_jwt_claims()

    if not jwt_claims.get("role") == "admin":
        return errors.access_not_allowed()

    member = Member.query.get(member_id)
    if not member:
        return errors.resource_not_found()

    invitation_code = InvitationCode(id=uuid.uuid4(), member=member)
    db.session.add(invitation_code)
    db.session.commit()

    return jsonify({"invitation_code": invitation_code.id}), 200
