import os
from flask import jsonify, request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jwt_claims,
    get_raw_jwt,
    jwt_refresh_token_required,
    jwt_required,
)
from itsdangerous import BadSignature, SignatureExpired
from openpatch_authentification.api.v1 import api
from openpatch_authentification.api.v1 import errors
from openpatch_authentification.jwt import blacklist_token
from openpatch_authentification.mail import (
    sEmail,
    send_confirm_email,
    send_reset_password_email,
    sPwd,
)
from openpatch_authentification.api.v1.schemas.policy import POLICIES_SCHEMA
from openpatch_authentification.models.member import Member
from openpatch_authentification.models.role import Role
from openpatch_authentification.models.policy import Policy
from openpatch_authentification.models.member_policy import MemberPolicy
from openpatch_authentification.models.invitation_code import InvitationCode
from openpatch_core.database import db
from sqlalchemy.exc import IntegrityError
from sqlalchemy import func, and_

BASE_URL = "/"


def is_valid_password(password):
    return len(password) >= 8 and len(password) <= 64


@api.route(BASE_URL + "login", methods=["POST"])
def login():
    """
    @api {post} /v1/login Login
    @apiVersion 1.0.0
    @apiName Login
    @apiGroup Root

    @apiParam {String} username The username or email.
    @apiParam {String} password The password.

    @apiSuccess {String} access_token An access token
    @apiSuccess {String} refresh_token A refresh token

    @apiUse errors_bad_username_or_password
    @apiUse errors_email_not_confirmed
    """
    json = request.get_json() or {}
    username = json.get("username", None)
    password = json.get("password", None)

    member = Member.query.filter_by(username=username).first()

    if not member:
        member = Member.query.filter_by(email=username).first()

    if member is None or not member.verify_password(password):
        return errors.bad_username_or_password()

    if not member.confirmed:
        return errors.email_not_confirmed()

    return jsonify(
        {
            "access_token": create_access_token(identity=member),
            "refresh_token": create_refresh_token(identity=member),
        }
    )


@api.route(BASE_URL + "register", methods=["POST"])
def register():
    """
    @api {post} /v1/register Register
    @apiVersion 1.0.0
    @apiName Register
    @apiGroup Root

    @apiParam {String} username The username
    @apiParam {String} password The password
    @apiParam {String} email An valid email
    @apiParam {String} [full_name] The full name
    @apiParam {String} [url="{}"] "{}" will be replace with confirmation token in email body

    @apiSuccess {String} message Waiting for confirmation of email address

    @apiUse errors_information_is_missing
    @apiUse errors_password_is_invalid
    @apiUse errors_username_is_in_use
    @apiUse errors_email_is_in_use
    @apiUse errors_email_is_invalid
    """

    json = request.get_json() or {}
    username = json.get("username", "")
    email = json.get("email", "")
    password = json.get("password", "")
    full_name = json.get("full_name", None)
    invitation_code_id = json.get("invitation_code", None)
    policy_id = json.get("policy")

    if os.getenv("OPENPATCH_DISABLE_REGISTRATION", False):
        invitation_code = InvitationCode.query.get(invitation_code_id)
        if invitation_code and invitation_code.used:
            return errors.invitation_code_used()
        elif invitation_code and not invitation_code.used:
            pass
        else:
            return errors.registration_disabled()

    invitation_code = InvitationCode.query.get(invitation_code_id)

    if username == "" or email == "" or password == "":
        return errors.information_is_missing()

    if not is_valid_password(password):
        return errors.password_is_invalid()

    if Member.query.filter_by(username=username).first() is not None:
        return errors.username_is_in_use()

    if Member.query.filter_by(email=email).first() is not None:
        return errors.email_is_in_use()

    role = Role.query.filter_by(type="user").first()
    member = Member(username=username, email=email, full_name=full_name, role=role)
    member.hash_password(password)

    url = json.get("url", "{}")
    send_confirm_email(email, url, username)

    db.session.add(member)

    if invitation_code:
        invitation_code.used = True

    if policy_id:
        member_policy = MemberPolicy(member=member, policy_id=policy_id, accepted=True)
        db.session.add(member_policy)

    db.session.commit()
    return jsonify({"message": "Waiting for confirmation of email address"}), 200


@api.route(BASE_URL + "refresh", methods=["GET"])
@jwt_refresh_token_required
def refresh():
    """
    @api {post} /v1/refresh Refresh access token
    @apiVersion 1.0.0
    @apiName Refresh
    @apiGroup Root

    @apiUse jwt

    @apiSuccess {String} access_token A new access token

    @apiUse errors_refresh_token_is_invalid
    """
    username = get_jwt_identity()
    member = Member.query.filter_by(username=username).first()

    if not member:
        return errors.refresh_token_is_invalid()

    res = {"access_token": create_access_token(identity=member)}
    return jsonify(res), 200


@api.route(BASE_URL + "logout", methods=["GET"])
def logout():
    """
    @api {post} /v1/logout Logout
    @apiVersion 1.0.0
    @apiName Logout
    @apiGroup Root
    """
    try:
        jti = get_raw_jwt()["jti"]
        blacklist_token(jti)
    except IntegrityError:
        pass
    except KeyError:
        pass
    return jsonify({"message": "Successfully logged out"}), 200


@api.route(BASE_URL + "verify", methods=["GET"])
@jwt_required
def verify():
    """
    @api {get} /v1/verify Verify access token
    @apiVersion 1.0.0
    @apiName Verify
    @apiGroup Root

    @apiUse jwt
    """

    return jsonify({"msg": "Verified"}), 200


@api.route(BASE_URL + "reset-password", methods=["GET", "POST"])
def reset_password():
    if request.method == "GET":
        return get_reset_password()
    # send email with one time password
    return post_reset_password()


def get_reset_password():
    """
    @api {get} /v1/reset-password Get reset password
    @apiVersion 1.0.0
    @apiName GetResetPassword
    @apiGroup Root

    @apiDescription The reset token is only valid for 1h.

    @apiParam {String} email The email address
    @apiParam {String} [url="{}"] "{}" will be replace with confirmation token in email body

    @apiUse errors_resource_not_found
    """
    email = request.args.get("email", None)
    url = request.args.get("url", {})
    member = Member.query.filter_by(email=email).first()

    if not member:
        return errors.resource_not_found()

    send_reset_password_email(email, url)

    return jsonify({"status": "success"}), 200


def post_reset_password():
    """
    @api {post} /v1/reset-password Post reset password
    @apiVersion 1.0.0
    @apiName PostResetPassword
    @apiGroup Root

    @apiParam {String} password The new password
    @apiParam {String} token The reset token

    @apiUse errors_token_is_invalid
    @apiUse errors_password_is_invalid
    @apiUse errors_information_is_missing
    @apiUse errors_resource_not_found
    """
    json = request.get_json() or {}
    password = json.get("password", None)
    token = json.get("token", None)

    # load token and check if valid
    email = ""
    try:
        email = sPwd.loads(token, max_age=3600)
    except (SignatureExpired, BadSignature):
        return errors.token_is_invalid()

    if not is_valid_password(password):
        return errors.password_is_invalid()

    if not token or not password:
        return errors.information_is_missing()

    member = Member.query.filter_by(email=email).first()

    if not member:
        return errors.resource_not_found()

    member.hash_password(password)
    db.session.commit()

    return jsonify({"status": "success"}), 200


@api.route(BASE_URL + "resend-email", methods=["POST"])
def resend_email():
    """
    @api {post} /v1/resend-email Resend email
    @apiVersion 1.0.0
    @apiName ResendEmail
    @apiGroup Root

    @apiParam {String} email The email address
    @apiParam {String} [url="{}"] "{}" will be replace with confirmation token in email body

    @apiUse errors_resource_not_found
    """
    email = request.json.get("email", None)
    url = request.json.get("url", {})
    member = Member.query.filter_by(email=email).first()

    if not member:
        return errors.resource_not_found()

    send_confirm_email(email, url)

    return jsonify({"status": "success"}), 200


@api.route(BASE_URL + "confirm-email", methods=["POST"])
def confirm_email():
    """
    @api {post} /v1/confirm-email Confirm Email
    @apiVersion 1.0.0
    @apiName ConfirmEmail
    @apiGroup Root

    @apiParam {String} token The confirmation token

    @apiUse errors_token_is_invalid
    @apiUse errors_resource_not_found
    """
    json = request.get_json() or {}
    token = json.get("token")

    try:
        email = sEmail.loads(token)  # , max_age=86400)
    except (SignatureExpired, BadSignature):
        return errors.token_is_invalid()

    member = Member.query.filter_by(email=email).first()

    if not member:
        return errors.resource_not_found()

    member.confirmed = True
    db.session.commit()

    return jsonify({"status": "success"})


@api.route(BASE_URL + "new-policies", methods=["GET"])
@jwt_required
def new_policies():
    """
    @api {post} /v1/new-policies New Policies
    @apiVersion 1.0.0
    @apiName NewPolicies
    @apiGroup Root

    @apiParam {UUID} id The id of a member

    @apiSuccess {Object[]} policies A list of new policies

    @apiUse errors_access_not_allowed
    """
    jwt_claims = get_jwt_claims()
    member = Member.query.get(jwt_claims.get("id"))

    if not member:
        return errors.access_not_allowed()

    latest_date_per_group_query = (
        db.session.query(Policy.type, func.max(Policy.created_on).label("latest_date"))
        .filter(Policy.draft == False)
        .group_by(Policy.type)
        .subquery("p2")
    )

    policy_query = db.session.query(Policy).join(
        latest_date_per_group_query,
        and_(
            Policy.type == latest_date_per_group_query.c.type,
            Policy.created_on == latest_date_per_group_query.c.latest_date,
        ),
    )

    latest_policies = policy_query.all()
    policies = []

    for latest_policy in latest_policies:
        member_policy = MemberPolicy.query.filter_by(
            policy=latest_policy, member=member
        ).first()
        if not member_policy or not member_policy.accepted:
            policies.append(latest_policy)

    return jsonify({"policies": POLICIES_SCHEMA.dump(policies)})
