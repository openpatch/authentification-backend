from openpatch_core.errors import error_manager as em


def access_not_allowed():
    """
    @apiDefine errors_access_not_allowed
    @apiError (ErrorCode 1) {Integer} code 1
    @apiError (ErrorCode 1) {Integer} status_code 403
    @apiError (ErrorCode 1) {String} message AccessNotAllowed
    """
    return em.make_json_error(
        403, message="Your credentials do not allow access to this resource", code=1
    )


def bad_authentification():
    """
    @apiDefine errors_bad_authentification
    @apiError (ErrorCode 2) {Integer} code 2
    @apiError (ErrorCode 2) {Integer} status_code 401
    @apiError (ErrorCode 2) {String} message BadAuthentification
    """
    return em.make_json_error(401, message="Bad authentification data", code=2)


def invalid_json(errors):
    """
    @apiDefine errors_invalid_json
    @apiError (ErrorCode 200) {String} message InvalidJson
    @apiError (ErrorCode 200) {Integer} status_code 400
    @apiError (ErrorCode 200) {Integer} code 200
    """
    return em.make_json_error(
        400, message="Invalid JSON in request body", code=200, details=errors
    )


def bad_username_or_password():
    """
    @apiDefine errors_bad_username_or_password
    @apiError (ErrorCode 100) {Integer} code 100
    @apiError (ErrorCode 100) {Integer} status_code 401
    @apiError (ErrorCode 100) {String} message BadUsernameOrPassword
    """
    return em.make_json_error(401, message="Bad username or password", code=100)


def email_not_confirmed():
    """
    @apiDefine errors_email_not_confirmed
    @apiError (ErrorCode 101) {String} message EmailNotConfirmed
    @apiError (ErrorCode 101) {Integer} status_code 422
    @apiError (ErrorCode 101) {Integer} code 101
    """
    return em.make_json_error(422, message="E-mail not confirmed", code=101)


def information_is_missing():
    """
    @apiDefine errors_information_is_missing
    @apiError (ErrorCode 102) {String} message InformationIsMissing
    @apiError (ErrorCode 102) {Integer} status_code 400
    @apiError (ErrorCode 102) {Integer} code 102
    """
    return em.make_json_error(400, message="Information is missing", code=102)


def password_is_invalid():
    """
    @apiDefine errors_password_is_invalid
    @apiError (ErrorCode 103) {String} message PasswordIsInvalid
    @apiError (ErrorCode 103) {Integer} status_code 400
    @apiError (ErrorCode 103) {Integer} code 103
    """
    return em.make_json_error(400, message="Password is not valid", code=103)


def username_is_in_use():
    """
    @apiDefine errors_username_is_in_use
    @apiError (ErrorCode 104) {String} message UsernameIsInUse
    @apiError (ErrorCode 104) {Integer} status_code 400
    @apiError (ErrorCode 104) {Integer} code 104
    """
    return em.make_json_error(400, message="Username is in use", code=104)


def email_is_in_use():
    """
    @apiDefine errors_email_is_in_use
    @apiError (ErrorCode 105) {String} message EmailIsInUse
    @apiError (ErrorCode 105) {Integer} status_code 400
    @apiError (ErrorCode 105) {Integer} code 105
    """
    return em.make_json_error(400, message="E-mail is in use", code=105)


def email_is_invalid():
    """
    @apiDefine errors_email_is_invalid
    @apiError (ErrorCode 106) {String} message EmailIsInvalid
    @apiError (ErrorCode 106) {Integer} status_code 400
    @apiError (ErrorCode 106) {Integer} code 106
    """
    return em.make_json_error(400, message="E-mail is invalid", code=106)


def access_token_is_invalid():
    """
    @apiDefine errors_access_token_is_invalid
    @apiError (ErrorCode 107) {String} message AccessTokenIsInvalid
    @apiError (ErrorCode 107) {Integer} status_code 401
    @apiError (ErrorCode 107) {Integer} code 107
    """
    return em.make_json_error(
        401, message="Access token is invalid or expired", code=107
    )


def refresh_token_is_invalid():
    """
    @apiDefine errors_refresh_token_is_invalid
    @apiError (ErrorCode 108) {String} message RefreshTokenIsInvalid
    @apiError (ErrorCode 108) {Integer} status_code 401
    @apiError (ErrorCode 108) {Integer} code 108
    """
    return em.make_json_error(
        401, message="Refresh token is invalid or expired", code=108
    )


def token_is_invalid():
    """
    @apiDefine errors_token_is_invalid
    @apiError (ErrorCode 109) {String} message TokenIsInvalid
    @apiError (ErrorCode 109) {Integer} status_code 401
    @apiError (ErrorCode 109) {Integer} code 109
    """
    return em.make_json_error(401, message="Token is invalid or expired", code=109)


def resource_not_found():
    """
    @apiDefine errors_resource_not_found
    @apiError (ErrorCode 110) {String} message ResourceNotFound
    @apiError (ErrorCode 110) {Integer} status_code 404
    @apiError (ErrorCode 110) {Integer} code 110
    """
    return em.make_json_error(404, message="Resource not found", code=110)


def need_one_admin():
    """
    @apiDefine errors_need_one_admin
    @apiError (ErrorCode 111) {String} message NeedOneAdmin
    @apiError (ErrorCode 111) {Integer} status_code 400
    @apiError (ErrorCode 111) {Integer} code 111
    """
    return em.make_json_error(400, message="One admin is needed", code=111)


def file_type_not_allowed():
    """
    @apiDefine errors_file_type_not_allowed
    @apiError (ErrorCode 112) {String} message FileTypeNotAllowed
    @apiError (ErrorCode 112) {Integer} status_code 400
    @apiError (ErrorCode 112) {Integer} code 112
    """
    return em.make_json_error(
        400, message="File type not allowed. Only images.", code=112
    )


def file_name_not_found():
    """
    @apiDefine errors_file_name_not_found
    @apiError (ErrorCode 113) {String} message FileNameNotFound
    @apiError (ErrorCode 113) {Integer} status_code 400
    @apiError (ErrorCode 113) {Integer} code 113
    """
    return em.make_json_error(400, message="File name is missing", code=113)


def file_is_too_big():
    """
    @apiDefine errors_file_is_too_big
    @apiError (ErrorCode 114) {String} message FileIsTooBig
    @apiError (ErrorCode 114) {Integer} status_code 400
    @apiError (ErrorCode 114) {Integer} code 114
    """
    return em.make_json_error(400, message="File is too big (max. 500kb)", code=114)


def registration_disabled():
    """
    @apiDefine errors_file_is_too_big
    @apiError (ErrorCode 115) {String} message RegistrationDisabled
    @apiError (ErrorCode 115) {Integer} status_code 400
    @apiError (ErrorCode 115) {Integer} code 115
    """
    return em.make_json_error(400, message="Registration is disabled", code=115)


def invitation_code_used():
    """
    @apiDefine errors_file_is_too_big
    @apiError (ErrorCode 116) {String} message InvitationCodeUsed
    @apiError (ErrorCode 116) {Integer} status_code 400
    @apiError (ErrorCode 116) {Integer} code 116
    """
    return em.make_json_error(400, message="Invitation Code used", code=116)


def new_policies():
    """
    @apiDefine errors_new_policies
    @apiError (ErrorCode 117) {String} message NewPolicies
    @apiError (ErrorCode 117) {Integer} status_code 451
    @apiError (ErrorCode 117) {Integer} code 117
    """
    return em.make_json_error(
        451,
        message="New Policies",
        code=117,
        details={
            "new_policies": {"method": "GET", "uri": "v1/new-policies"},
            "accept_policy": {"method": "POST", "uri": "v1/policies/[id]/accept"},
        },
    )
