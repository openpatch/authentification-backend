from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_template.models.member import Role


class RoleSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Role
        load_instance = True

    id = fields.UUID()


ROLE_SCHEMA = RoleSchema()
ROLES_SCHEMA = RoleSchema(many=True)
