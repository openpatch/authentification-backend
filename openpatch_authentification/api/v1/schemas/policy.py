from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_authentification.models.policy import Policy


class PolicySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Policy
        exclude = ["member_policy"]
        load_instance = True

    id = fields.UUID()


POLICY_SCHEMA = PolicySchema()
POLICIES_SCHEMA = PolicySchema(many=True)
