from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_template.models.member import Member


class MemberSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Member
        load_instance = True

    id = fields.UUID()


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
