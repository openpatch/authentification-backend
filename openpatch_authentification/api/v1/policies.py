from flask import jsonify, request
from flask_jwt_extended import get_jwt_claims, jwt_required
from marshmallow import ValidationError

from openpatch_authentification.api.v1 import api
from openpatch_authentification.api.v1 import errors
from openpatch_authentification.api.v1.schemas.policy import (
    POLICY_SCHEMA,
    POLICIES_SCHEMA,
)
from openpatch_authentification.models.policy import Policy
from openpatch_authentification.models.member import Member
from openpatch_authentification.models.member_policy import MemberPolicy
from openpatch_core.database import db

BASE_URL = "/policies"


@api.route(BASE_URL, methods=["POST", "GET"])
@jwt_required
def policies():
    jwt_claims = get_jwt_claims()

    if jwt_claims.get("role") != "admin":
        return errors.access_not_allowed()

    if request.method == "POST":
        return post_policies()
    elif request.method == "GET":
        return get_policies()


def post_policies():
    policy_json = request.get_json()
    if not policy_json:
        return errors.no_json()

    try:
        policy = POLICY_SCHEMA.load(policy_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(policy)
    db.session.commit()

    return jsonify({"policy_id": policy.id})


def get_policies():
    policy_query, count, page = Policy.elastic_query(request.args.get("query", "{}"))
    policy_query = page(policy_query)
    policies = policy_query.all()

    return jsonify({"policies": POLICIES_SCHEMA.dump(policies)})


@api.route(BASE_URL + "/latest", methods=["GET"])
def policy_latest():
    type = request.args.get("type", None)
    lang = request.args.get("lang", "en")
    policy = Policy.query.filter_by(type=type, language=lang, draft=False).first()

    if not policy:
        return errors.resource_not_found()

    return jsonify({"policy": POLICY_SCHEMA.dump(policy)})


@api.route(BASE_URL + "/<policy_id>", methods=["GET", "PUT"])
def policy_by_id(policy_id):
    if request.method == "GET":
        return get_policy_by_id(policy_id)
    elif request.method == "PUT":
        return put_policy_by_id(policy_id)


def get_policy_by_id(policy_id):
    policy = Policy.query.get(policy_id)

    if not policy:
        return errors.resource_not_found()

    return jsonify({"policy": POLICY_SCHEMA.dump(policy)})


@jwt_required
def put_policy_by_id(policy_id):
    jwt_claims = get_jwt_claims()

    policy_json = request.get_json()
    if not policy_json:
        return errors.no_json()

    if jwt_claims.get("role") != "admin":
        return errors.access_not_allowed()

    policy = Policy.query.get(policy_id)

    try:
        result = POLICY_SCHEMA.load(
            policy_json, session=db.session, instance=policy, partial=True
        )
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.commit()

    return jsonify({}), 200


@api.route(BASE_URL + "/<policy_id>/accept", methods=["POST", "DELETE"])
@jwt_required
def policy_accept(policy_id):
    if request.method == "POST":
        return post_policy_accept(policy_id)
    elif request.method == "DELETE":
        return delete_policy_accept(policy_id)


def post_policy_accept(policy_id):
    jwt_claims = get_jwt_claims()

    member = Member.query.get(jwt_claims.get("id"))
    policy = Policy.query.get(policy_id)

    if not member or not policy:
        return errors.resource_not_found()

    member_policy = MemberPolicy.query.filter_by(member=member, policy=policy).first()

    if not member_policy:
        member_policy = MemberPolicy(member=member, policy=policy)

    member_policy.accepted = True

    db.session.add(member_policy)
    db.session.commit()

    return jsonify({}), 200


def delete_policy_accept(policy_id):
    jwt_claims = get_jwt_claims()

    member = Member.query.get(jwt_claims.get("id"))
    policy = Policy.query.get(policy_id)

    if not member or not policy:
        return errors.resource_not_found()

    member_policy = MemberPolicy.query.filter_by(member=member, policy=policy).first()

    if not member_policy:
        return jsonify({}), 200

    member_policy.accepted = False
    db.session.commit()

    return jsonify({}), 200
