import os


class Config(object):
    """Parent configuration class."""

    DEBUG = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.getenv("OPENPATCH_DB")
    SECRET_KEY = os.getenv("OPENPATCH_SECRET")
    JWT_SECRET_KEY = os.getenv("OPENPATCH_JWT")


class DevelopmentConfig(Config):
    """Configurations for Development."""

    DEBUG = True
    JWT_REFRESH_TOKEN_EXPIRES = False


class TestingConfig(Config):
    """Configurations for Testing, with a separate test database."""

    TESTING = True
    DEBUG = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class StagingConfig(Config):
    """Configurations for Staging."""

    DEBUG = True


class ProductionConfig(Config):
    """Configurations for Production."""

    DEBUG = False
    TESTING = False


app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "staging": StagingConfig,
    "production": ProductionConfig,
}
